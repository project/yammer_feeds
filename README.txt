
CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Settings
* Contact

INTRODUCTION
------------
This module will enables you to show your  yammer feeds shown within a block. First time the block
will take time to load, It will be loaded from cache unless either 2700 sec is not passed or user 
explictly does press refresh button shown within the yammer feeds block.

REQUIREMENTS
------------
1. PHP 5.2 or higher.
2. Drupal 6.x.
3. OAuth PHP Library: http://oauth.googlecode.com/svn/code/php/OAuth.php


INSTALLATION
------------
1. Download OAuth PHP Library: http://oauth.googlecode.com/svn/code/php/OAuth.php
   and place it in the folder [module_folder]/yammer_feeds/api

2. Enable Curl :
  cUrl should be enabled. Open php.ini and change the statement
  ';extension=php_curl.dll' to 'extension=php_curl.dll' (quotes for clarity)

3. Restart your server.

4. Go to http://oauth.googlecode.com/svn/code/php/OAuth.php and download OAuth.php
   and place it in the path [modules_folder]/yammer_feeds/api

5. The cache is required for this module to be enabled.

6. Make sure "clean" URLs (i.e. without ?q= in the URL) are enabled (admin/settings/clean-urls)
   
   IMPORTANT: This module can only be used by authenticated users so please be careful
   while assigning rights to anonymous users.

SETTINGS
--------
Step 1: Create Yammer Application
  i.  Create a Yammer Application https://www.yammer.com/client_applications/new
 ii.  After successful creation you will prompt with consumer keys and consumer secret of your application.

Step 2: Go to the path (/admin/settings/yammer-feeds) Or Configuration -> yammer_feeds
  i.  Copy these values and paste it on appropriate fields on the path (/admin/settings/yammer-feeds)

Step 3: Go to the path (admin/build/block/list)
  i. Enable the block “Yammer Feeds�? in an appropriate place as required.
 ii. Go to any of the page where block is enable.
iii. Click on “Settings�? Link within block  (This will take you on the page located at path: yammer-feeds/yammer-verify-form)

Step 4: Verify your application
  i. Get Verification Code
 ii. Enter the verification code in the field below and submit it

NOTE:
-----
CASE 1: To Load yammer feeds for another application or user.
  i. Go to the path /admin/settings/yammer-feeds and click on 'Reset to Default' button and follow step 1 through 4


CASE 2: If yammer feeds are not shown in the block        
  i. Go to the path /admin/settings/yammer-feeds and click on 'Reset to Default' button and follow step 2 through 4

		
CONTACT
-------
Confiz Solutions: http://www.confiz.com
Developer Contact: shahzadi.samia@confiz.com
