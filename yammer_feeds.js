
Drupal.behaviors.yammer_feeds = function(context) {
  $('#more-link-id').click(function() {
    $("#loading-image-block").html('<div class="loading-image">&nbsp;</div>');
    $.get('?q=yammer-feeds/older-messages/ajax', null,  more_messages_callback);
  });
  var more_messages_callback = function(response) {
    var result = Drupal.parseJson(response);
    $("#loading-image-block").empty();
    $("#more-messages").append(result.message);
  };
  $('#referesh-link-id').click(function() {
    $('#msg').empty();
    $('#msg').html('<div class="pre-loading-image"></div>');
    $.get('?q=yammer-feeds/newer-messages/ajax', null,  newer_messages_callback);
  });
  var newer_messages_callback = function(response) {
    var result = Drupal.parseJson(response);
    $('#msg').html(result.message);
  };
};